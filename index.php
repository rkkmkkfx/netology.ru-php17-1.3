<?php
$data = [
  'Eurasia' => ['Procyon lotor', 'Vulpes bengalensis', 'Vulpes'],
  'Africa' => ['Proteles cristata', 'Panthera pardus', 'Herpestidae'],
  'Australia' => ['Setonix brachyurus', 'Sarcophilus harrisii', 'Vombatidae'],
  'North America' => ['Panthera onca', 'Dasypus novemcinctus', 'Felis pardalis', 'Canis'],
  'Antarctica' => ['Hydrurga leptonyx', 'Aptenodytes forsteri']
];

$selected_animals = [];

foreach ($data as $continent) {
  foreach ($continent as $animal) {
    if (strpos($animal, ' ')) {
      $selected_animals[] = $animal;
    }
  }
}

$selected_animals_array = [];

foreach ($selected_animals as $animal) {
  $selected_animals_array[] = explode(' ', $animal);
}

$first = $second = [];

foreach ($selected_animals_array as $array) {
  $first[] = $array[0];
  $second[] = $array[1];
}
shuffle($first);
shuffle($second);

$created_animals = [];

for ($i = 0; $i < count($selected_animals); $i++) {
  $created_animals[] = implode(' ', [$first[$i], $second[$i]]);
}

print_r($created_animals);